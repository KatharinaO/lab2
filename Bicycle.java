//Katharina Orfanidis 1842297
public class Bicycle {
    //create fields
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    //create constructor
    public Bicycle (String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    //get methods
    public String getManu() {
        return this.manufacturer; 
    }

    public int getGears() {
        return this.numberGears;
    }

    public double getSpeed() {
        return this.maxSpeed;
    }
    //to string
    public String toString(){
        return "Manufacturer: "+this.manufacturer+"\nnumber of gears: "+this.numberGears+"\nMax Speed: "+this.maxSpeed;
    }
}