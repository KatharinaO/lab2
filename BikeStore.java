//Katharina Orfanidis 1842297
public class BikeStore {  
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("RALEIGH", 12, 35);
        bikes[1] = new Bicycle("GHOST", 5, 25);
        bikes[2] = new Bicycle("devinci", 20, 45);
        bikes[3] = new Bicycle("PREVELO", 15, 30);

        for(int i = 0; i < bikes.length; i++){
            String bike = bikes[i].toString();
            System.out.println(bike);
        }
    }
}
